package abexa.beesystem.payments.android.api.readqr.entities;

public class Tickets {
    public double p;
    public int q;
    public String c;
    public String d;


    public double getP() {
        return p;
    }

    public void setP(double p) {
        this.p = p;
    }

    public int getQ() {
        return q;
    }

    public void setQ(int q) {
        this.q = q;
    }

    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }
}


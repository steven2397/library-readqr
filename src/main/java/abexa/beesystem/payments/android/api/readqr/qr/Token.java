package abexa.beesystem.payments.android.api.readqr.qr;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import abexa.beesystem.payments.android.api.readqr.BuildConfig;
import abexa.beesystem.payments.android.api.readqr.async.GetTokenTask;
import abexa.beesystem.payments.android.api.readqr.entities.request.TokenPasswordRequestDto;
import abexa.beesystem.payments.android.api.readqr.spf.SPF;
import okhttp3.internal.Util;

public class Token {

    public void getToken(final Context context){

        TokenPasswordRequestDto tokenPasswordRequestDto = new TokenPasswordRequestDto(
                BuildConfig.USERNAME,
                BuildConfig.PASSWORD,
                BuildConfig.CLIENT_ID,
                BuildConfig.CLIENT_SECRET,
                BuildConfig.URL_BASE + BuildConfig.CONNECT_TOKEN
        );

        new GetTokenTask(context, tokenPasswordRequestDto, new GetTokenTask.IGetTokenResult() {
            @Override
            public void onSuccesfull(String accesToken) {
                SPF.getInstance(context).saveTokenSharedPreferences(accesToken);
                Log.d("Testing","TOKEN : "+accesToken);
            }
            @Override
            public void onError(String msg) {
            }
        }).execute();
    }
}

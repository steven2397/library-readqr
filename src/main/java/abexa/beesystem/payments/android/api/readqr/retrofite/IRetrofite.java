package abexa.beesystem.payments.android.api.readqr.retrofite;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface IRetrofite {

    @POST("api/Transaction/CollectionRegister")
    Call<ResponseBody> CollectionRegister(@Body RequestBody requestBody);
}

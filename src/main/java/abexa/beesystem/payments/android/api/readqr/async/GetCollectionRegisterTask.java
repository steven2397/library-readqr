package abexa.beesystem.payments.android.api.readqr.async;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.util.List;

import abexa.beesystem.payments.android.api.readqr.R;
import abexa.beesystem.payments.android.api.readqr.constants.Constants;
import abexa.beesystem.payments.android.api.readqr.entities.OperationResult;
import abexa.beesystem.payments.android.api.readqr.entities.response.CollectionRegisterResponseDTO;
import abexa.beesystem.payments.android.api.readqr.retrofite.ConnectionRetrofit;
import abexa.beesystem.payments.android.api.readqr.retrofite.IRetrofite;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;



public class GetCollectionRegisterTask extends AsyncTask<String, Void, String>
{

    private Context context;
    private RequestBody requestBody;
    private Retrofit retrofit;
    private IGetRegisterTask iGetRegisterTask;


    public interface IGetRegisterTask {
        void onResult(OperationResult<List<CollectionRegisterResponseDTO>> response);
        void onError(String string);
    }

    public  GetCollectionRegisterTask(Context context, RequestBody requestBody, IGetRegisterTask iGetRegisterTask) {
        this.context = context;
        this.requestBody = requestBody;
        this.retrofit = ConnectionRetrofit.APIHOST(context, Constants.getHostPublica());
        this.iGetRegisterTask = iGetRegisterTask;
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            IRetrofite iRetrofite = retrofit.create(IRetrofite.class);
            Call<ResponseBody> call =  iRetrofite.CollectionRegister(requestBody);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        Log.i("Collection","Entro al Onresponse");
                        if (response.isSuccessful() && response.body() != null){
                            String json = response.body().string();
                            Log.i("Collection","Response ok");
                            Log.i("Collection",json);

                            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
                            OperationResult<List<CollectionRegisterResponseDTO>> operationResultRegister= gson.fromJson(json,
                                    new TypeToken<OperationResult<List<CollectionRegisterResponseDTO>>>(){}.getType());
                            String responseOut = response.body().string();
                            iGetRegisterTask.onResult(operationResultRegister);
                        }else{
                            iGetRegisterTask.onError(context.getResources().getString(R.string.error_body));
                        }

                    } catch (Exception e) {
                        iGetRegisterTask.onError(context.getResources().getString(R.string.error_internal_1));
                    }
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    iGetRegisterTask.onError(context.getResources().getString(R.string.error_connection));
                }
            });
        }catch (Exception e){
            iGetRegisterTask.onError(context.getResources().getString(R.string.error_internal_2));
        }
        return null;
    }
}

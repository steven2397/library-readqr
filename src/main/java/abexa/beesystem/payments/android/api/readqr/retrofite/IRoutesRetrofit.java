package abexa.beesystem.payments.android.api.readqr.retrofite;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Jhon Coronel
 */
public interface IRoutesRetrofit {

    /**
     * Con fin educativo, probando la seguridad de la página SerPost Perú
     * Tenemos que consulta 3 EndPoints
     * @param body
     * @return
     */
    @Headers("Content-Type: application/json")
    @POST("/prj_online/Web_Busqueda.aspx/Consultar_Tracking")
    Call<ResponseBody> Consultar_Tracking(@Body RequestBody body);

    /**
     * Con fin educativo, probando la seguridad de la página SerPost Perú
     * Tenemos que consulta 3 EndPoints
     * @param body
     * @return
     */
    @Headers("Content-Type: application/json")
    @POST("/prj_online/Web_Busqueda.aspx/Consultar_Tracking_Detalle_IPS")
    Call<ResponseBody> Consultar_Tracking_Detalle_IPS(@Body RequestBody body);



    
}

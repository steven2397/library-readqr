package abexa.beesystem.payments.android.api.readqr.scan;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import abexa.beesystem.payments.android.api.readqr.activities.Popup;
import abexa.beesystem.payments.android.api.readqr.entities.CaptureAct;
import abexa.beesystem.payments.android.api.readqr.entities.request.CollectionRegisterRequestDto;


public class ScanHelper {

    public CallBNackResult callBNackResult;
    public CollectionRegisterRequestDto collectionregister;
    public static ScanHelper scanHelper = null;
    public static ScanHelper getInstance()
    {
        if (scanHelper == null)
            scanHelper = new ScanHelper();
        return scanHelper;
    }

    public void setCallBNackResult(CallBNackResult callBNackResult)
    {
        this.callBNackResult = callBNackResult;
    }

    public interface CallBNackResult{
        void onResult(String result);
    }

    public void scan(Activity activity){
        IntentIntegrator integrator = new IntentIntegrator(activity);
        integrator.setCaptureActivity(CaptureAct.class);
        integrator.setOrientationLocked(false);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES); //Establecer formatos de codigos
        integrator.setPrompt("Scanning Code");
        integrator.initiateScan();
    }

    public  IntentResult parseActivityResult(int requestCode, int resultCode, Intent intent) {
        return IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data, Context context) {
        IntentResult result = parseActivityResult(requestCode, resultCode, data);
        if(result != null){
            if (result.getContents() != null){
                final AlertDialog.Builder builder =  new AlertDialog.Builder(context);
                builder.setMessage(result.getContents());
                builder.setTitle("Scanning Result");
                builder.setPositiveButton("Scan Again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    };
                });
                //collectionregister.qrJson = result.getContents();
//                Popup alert = new Popup();
//                alert.showDialog(context);
                callBNackResult.onResult(result.getContents());
            }
            else{
                Toast.makeText(context, "No Results",Toast.LENGTH_LONG).show();
            }
        }
        else{
            //super.onActivityResult(requestCode,resultCode,data);
        }
    }

}

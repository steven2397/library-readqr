package abexa.beesystem.payments.android.api.readqr.views;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.CountDownTimer;
import android.text.Spanned;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.Nullable;
import abexa.beesystem.payments.android.api.readqr.R;

public class AbexaQRView extends LinearLayout implements View.OnClickListener{
    //Layout Original
    private LayoutInflater inflater;
    //Container
    private RelativeLayout rlContainerQR;
    //Vistas
    private ImageView ivAcept;
    private ImageView ivRestore;
    private ImageView ivCancel;
    private TextView tvTotal;

    //Transaction Status
    private LinearLayout llQrFast;
    private RelativeLayout rlButtonBarQrFast;
    private TextView tvState;
    private ImageView ivQrFast;


    private IAbexaQRView iAbexaQRView;

    public static boolean isQrActive = false;

    public CountDownTimer countDownTimer;

    public interface IAbexaQRView{
        void onClickAccept();
        void onClickRestore();
        void onClickCancel();
    }

    public void setIAbexaQRView(IAbexaQRView iAbexaQRView){
        this.iAbexaQRView = iAbexaQRView;
    }

    public AbexaQRView(Context context) {
        super(context);
        if (isInEditMode()) {
            init();
        }
    }

    public AbexaQRView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initViews(context);
        initListeners();
        if(isInEditMode()){
            init();
        }
    }

    public AbexaQRView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(isInEditMode()){
            init();
        }
    }

    private void init(){
        inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.abexa_qr_view_original, this, true);
    }

    private void initViews(Context context){
        //
        inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.abexa_qr_view_original, this, true);
        rlContainerQR = (RelativeLayout)view.findViewById(R.id.rlContainerQR);
        ivAcept = (ImageView) view.findViewById(R.id.ivAcept);
        ivRestore = (ImageView) view.findViewById(R.id.ivRestore);
        ivCancel = (ImageView) view.findViewById(R.id.ivCancel);
        tvTotal = (TextView) view.findViewById(R.id.tvTotal);

        //Transaction Status
        llQrFast = (LinearLayout) view.findViewById(R.id.llQrFast);
        rlButtonBarQrFast = (RelativeLayout) view.findViewById(R.id.rlButtonBarQrFast);
        tvState = (TextView) view.findViewById(R.id.tvState);
        ivQrFast = (ImageView) view.findViewById(R.id.ivQrFast);

    }

    private void initListeners(){
        ivAcept.setOnClickListener(this);
        ivRestore.setOnClickListener(this);
        ivCancel.setOnClickListener(this);
    }

    public void showQrDetected(){
        isQrActive = true;
        ivQrFast.setAlpha(0.5f);
        rlContainerQR.setVisibility(VISIBLE);
        rlButtonBarQrFast.setVisibility(VISIBLE);



    }

    public void hideQrDetected(){
        isQrActive = false;
        rlContainerQR.setVisibility(INVISIBLE);
        tvState.setVisibility(GONE);
        ivQrFast.setImageDrawable(getResources().getDrawable(R.drawable.qfast2));
        llQrFast.setBackground(null);
        this.setAmountTotal(0);
    }

    public void setAmountTotal(double amountTotal){
        @SuppressLint("DefaultLocale")
        String amounTotalFormat = String.format("Total: %.2f", amountTotal);

        tvTotal.setText(amounTotalFormat);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.ivAcept) {
            this.actionAccept();
        } else if(id == R.id.ivRestore){
            this.actionRestore();
        }
        else if (id == R.id.ivCancel) {
            this.acctionCancel();
        }
    }

    private void actionAccept(){
        //your code

        if(iAbexaQRView !=  null)
            iAbexaQRView.onClickAccept();
    }

    private void actionRestore(){
        //your code

        if(iAbexaQRView !=  null)
            iAbexaQRView.onClickRestore();;
    }

    private void acctionCancel(){
        //your code

        if(iAbexaQRView !=  null)
            iAbexaQRView.onClickCancel();
    }

    /**
     *
     * @param message
     */
    public void showTransactionStatus(boolean isSuccesfull, Spanned message){

        ivQrFast.setAlpha(1f);
        if (isSuccesfull)
        {
            ivQrFast.setImageDrawable(getResources().getDrawable(R.drawable.ic_correcto_256));
            tvState.setTextColor(getResources().getColor(R.color.verde));
        }
        else{
            ivQrFast.setImageDrawable(getResources().getDrawable(R.drawable.ic_error_256));
            tvState.setTextColor(getResources().getColor(R.color.rojo));
        }
        tvState.setVisibility(VISIBLE);

        llQrFast.setBackground(getResources().getDrawable(R.drawable.shape_bg_dialog));
        tvState.setText(message);
        rlButtonBarQrFast.setVisibility(INVISIBLE);
        timerhideTransaction();
    }
    private void timerhideTransaction(){
        if(countDownTimer == null){
            countDownTimer = new CountDownTimer(2000,1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    hideQrDetected();
                }
            }.start();
        }
        else
        {
            countDownTimer.start();
        }
    }

}

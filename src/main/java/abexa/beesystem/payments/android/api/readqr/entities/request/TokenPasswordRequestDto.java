package abexa.beesystem.payments.android.api.readqr.entities.request;

public class TokenPasswordRequestDto {

    private String username = "";
    private String password = "";
    private String client_id = "";
    private String client_secret = "";
    private String url_connect_token = "";

    public TokenPasswordRequestDto() {
    }

    public TokenPasswordRequestDto(String username, String password, String client_id, String client_secret, String url_connect_token) {
        this.username = username;
        this.password = password;
        this.client_id = client_id;
        this.client_secret = client_secret;
        this.url_connect_token = url_connect_token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }

    public String getUrl_connect_token() {
        return url_connect_token;
    }

    public void setUrl_connect_token(String url_connect_token) {
        this.url_connect_token = url_connect_token;
    }
}

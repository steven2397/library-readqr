package abexa.beesystem.payments.android.api.readqr.entities;

public class Host {

    private String host = "";
    private int puerto = 0;

    public void setHost(String host) {
        this.host = host;
    }

    public void setPuerto(int puerto) {
        this.puerto = puerto;
    }

    /**
     * @param host String
     * @param puerto int
     */
    public Host(String host, int puerto) {
        this.host = host;
        this.puerto = puerto;
    }

    public Host() {
    }

    public String getHost() {
        return host;
    }

    public int getPuerto() {
        return puerto;
    }

}

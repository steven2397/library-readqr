package abexa.beesystem.payments.android.api.readqr.entities.request;
import java.util.ArrayList;
import abexa.beesystem.payments.android.api.readqr.entities.Tickets;

public class CollectionRegisterRequestDto {


    public String deviceCode;
    public String qrJson;
    public String type;
    public double amount;
    public String rate;
    public ArrayList<Tickets> detail;

    public CollectionRegisterRequestDto()
    {

    }

    public CollectionRegisterRequestDto(String deviceCode, String qrJson, String type, double amount, String rate, ArrayList<Tickets> detail) {
        this.deviceCode = deviceCode;
        this.qrJson = qrJson;
        this.type = type;
        this.amount = amount;
        this.rate = rate;
        this.detail = detail;
    }



}

package abexa.beesystem.payments.android.api.readqr.constants;
import abexa.beesystem.payments.android.api.readqr.entities.Host;

public class Constants {


    public final static String IPPUBLICA = "beequeenwebapi.azurewebsites.net";

    public final static int CODRESULTADOVACIO = 0;
    public final static int PUERTOHTTP = 0;
    public final static int PUERTOWS   = 0;
    public final static int RESULT_OK = 1;
    public final static int RESULT_ERROR_CONNECTION = 2;
    public final static int RESULT_ERROR_CERO = 0;
    public final static int RESULT_ERROR = -1;
    public final static int RESULT_WARNING  = 3;


    public final static int ACTIVO = 1;
    public final static int INACTIVO = 2;

    public final static String SEPBV = "|";
    public final static String SEPSN = "~";
    public final static String SEPAS = "*";
    public final static String SECOM = ",";

    public final static String DESA = "#";

    public final static String DESBV = "\\" + SEPBV;   // '\\|'
    public final static String DESAS = "\\" + SEPAS;
    public final static String DESSLASH = "\\/";

    public final static int BAJA    = 1;    // prioridades para los indicadores
    public final static int MEDIA   = 2;
    public final static int ALTA    = 3;

    public final static int CODHABILITADO = 1;
    public final static int CODDESHABILITADO = 0;

    //añadidos
    public final static int FLAG_LOCAL = 0;
    public final static int FLAG_ENVIADO = 1;
    public final static int COD_SQL = 0;

    //

    public static Host getHostPublica(){
        Host host = new Host();
        host.setHost(Constants.IPPUBLICA);
        return host;
    }

}

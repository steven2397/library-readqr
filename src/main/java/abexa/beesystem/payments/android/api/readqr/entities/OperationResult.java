package abexa.beesystem.payments.android.api.readqr.entities;

public class OperationResult<T>
{
    public boolean isValid;
    public OperationException[] exceptions;
    public T content;

    public class OperationException
    {
        public String code;
        public String description;
        public OperationException(String code, String description)
        {
            this.code = code;
            this.description = description;
        }
    }
}



package abexa.beesystem.payments.android.api.readqr.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import abexa.beesystem.payments.android.api.readqr.R;

public class AbexaQRView3 extends LinearLayout implements View.OnClickListener{
    //Layout view 3
    private LayoutInflater inflater;
    //Container
    private RelativeLayout rlContainerQR;
    //Vistas
    private Button btnAcept;
    private Button btnCancell;
    private Button btnRestore;
    private TextView tvTotal;


    private IAbexaQRView iAbexaQRView;

    public static boolean isQrActive = false;

    public interface IAbexaQRView{
        void onClickAccept();
        void onClickRestore();
        void onClickCancel();
    }

    public void setIAbexaQRView(IAbexaQRView iAbexaQRView){
        this.iAbexaQRView = iAbexaQRView;
    }

    public AbexaQRView3(Context context) {
        super(context);
        if (isInEditMode()) {
            init();
        }
    }

    public AbexaQRView3(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initViews(context);
        initListeners();
        if(isInEditMode()){
            init();
        }
    }

    public AbexaQRView3(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(isInEditMode()){
            init();
        }
    }

    private void init(){
        inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.abexa_qr_view3, this, true);
    }

    private void initViews(Context context){
        inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.abexa_qr_view3, this, true);
        rlContainerQR = (RelativeLayout)view.findViewById(R.id.rlContainerQR);
        //View
        btnAcept = (Button) view.findViewById(R.id.btnAccept);
        btnCancell = (Button) view.findViewById(R.id.btnCancel);
        btnRestore = (Button) view.findViewById(R.id.btnRestore);
        tvTotal = (TextView) view.findViewById(R.id.tvTotal);
    }

    private void initListeners(){
        btnAcept.setOnClickListener(this);
        btnRestore.setOnClickListener(this);
        btnCancell.setOnClickListener(this);
    }

    public void showQrDetected(){
        isQrActive = true;
        rlContainerQR.setVisibility(VISIBLE);
    }

    public void hideQrDetected(){
        isQrActive = false;
        rlContainerQR.setVisibility(INVISIBLE);
    }

    public void setAmountTotal(double amountTotal){
        @SuppressLint("DefaultLocale")
        String amounTotalFormat = String.format("Total: %.2f", amountTotal);

        tvTotal.setText(amounTotalFormat);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btnAccept) {
            this.actionAccept();
        } else if(id == R.id.btnRestore){
            this.actionRestore();
        }
        else if (id == R.id.btnCancel) {
            this.acctionCancel();
        }
    }

    private void actionAccept(){
        //your code
        if(iAbexaQRView !=  null)
            iAbexaQRView.onClickAccept();
    }

    private void actionRestore(){
        //your code
        if(iAbexaQRView !=  null)
            iAbexaQRView.onClickRestore();;
    }

    private void acctionCancel(){
        //your code
        if(iAbexaQRView !=  null)
            iAbexaQRView.onClickCancel();
    }

}

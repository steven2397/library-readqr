package abexa.beesystem.payments.android.api.readqr.qr;

import android.content.Context;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.List;

import abexa.beesystem.payments.android.api.readqr.async.GetCollectionRegisterTask;
import abexa.beesystem.payments.android.api.readqr.entities.OperationResult;
import abexa.beesystem.payments.android.api.readqr.entities.Tickets;
import abexa.beesystem.payments.android.api.readqr.entities.request.CollectionRegisterRequestDto;
import abexa.beesystem.payments.android.api.readqr.entities.response.CollectionRegisterResponseDTO;
import okhttp3.MediaType;
import okhttp3.RequestBody;


public class CollectionRegister {

    private CollectionRegisterRequestDto transaction;
    private CollectionRegisterRequestDto registerRequestDto;
    private Context context;
    private ArrayList<Tickets> allTicket;
//    private CountDownTimer countDownTimer;


    public IColecctionRegister iColecctionRegister;
    public interface IColecctionRegister{
        void onSucess(boolean isValid, String msgError);
        void onError(String msgError);
    }

    public CollectionRegister(Context context, CollectionRegisterRequestDto transaction, ArrayList<Tickets> allTicket, IColecctionRegister iColecctionRegister)
    {
        this.context = context;
         this.transaction = transaction;
         this.allTicket = allTicket;
         this.iColecctionRegister = iColecctionRegister;
         registerCollection();
    }

    private void registerCollection()
    {
        registerRequestDto = new CollectionRegisterRequestDto();
        registerRequestDto.deviceCode = transaction.deviceCode;
        registerRequestDto.qrJson = transaction.qrJson;
        registerRequestDto.type = transaction.type;
        registerRequestDto.amount = transaction.amount;
        registerRequestDto.rate = transaction.rate;
        registerRequestDto.detail = allTicket;

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        Gson gson = gsonBuilder.create();
        String jsonRequest = gson.toJson(registerRequestDto);
        Log.d("Testing", jsonRequest);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonRequest);
        new GetCollectionRegisterTask(context, requestBody, new GetCollectionRegisterTask.IGetRegisterTask() {
            @Override
            public void onResult(OperationResult<List<CollectionRegisterResponseDTO>> operationResultRegister) {
                if(!operationResultRegister.isValid){
                    iColecctionRegister.onSucess(false, "Transaction Error");
                    return;
                }
                if(operationResultRegister.exceptions != null){
                    String error = "";
                    for (OperationResult.OperationException ex:operationResultRegister.exceptions) {
                        error = ex.description;
                    }
                    iColecctionRegister.onSucess(false, error);
                    return;
                }
                iColecctionRegister.onSucess(true, "Succesfull Transaction OK");
            }

            @Override
            public void onError(String string) {
                iColecctionRegister.onError(string);
            }
        }).execute();
    }
}

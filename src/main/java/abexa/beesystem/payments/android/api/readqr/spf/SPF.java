package abexa.beesystem.payments.android.api.readqr.spf;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;


@SuppressLint("ApplySharedPref")
public class SPF {

    private static SPF instance = null;
    private Context context = null;
    private SharedPreferences.Editor editorSPF = null;
    private SharedPreferences spf = null;
    private String TOKEN = "TOKEN";


    private SPF(Context context)
    {
        this.context = context;
        spf = this.context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
    }

    public static SPF getInstance(Context context)
    {
        if (instance == null)
            instance = new SPF(context);
        return instance;
    }
    public void saveTokenSharedPreferences(String token)
    {

        editorSPF = spf.edit();
        editorSPF.putString(TOKEN, token);
        editorSPF.commit();
    }

    public String getKeyToken()
    {
        return spf.getString(TOKEN, "");
    }



}

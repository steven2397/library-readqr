package abexa.beesystem.payments.android.api.readqr.entities.response;


import java.util.Date;

public class CollectionRegisterResponseDTO {
    public String account;
    public double available;
    public float balance;
    public Date performedDate;
}

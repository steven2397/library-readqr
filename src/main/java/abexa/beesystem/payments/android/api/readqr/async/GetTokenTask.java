package abexa.beesystem.payments.android.api.readqr.async;
import android.content.Context;
import android.os.AsyncTask;
import java.io.IOException;
import abexa.beesystem.payments.android.api.readqr.entities.request.TokenPasswordRequestDto;
import ca.mimic.oauth2library.OAuth2Client;
import ca.mimic.oauth2library.OAuthError;
import ca.mimic.oauth2library.OAuthResponse;
import retrofit2.Retrofit;

public class GetTokenTask extends AsyncTask<String, Void, String>
{
    private Context context;
    private Retrofit retrofit;
    private TokenPasswordRequestDto tokenPasswordRequestDto;
    private IGetTokenResult iGetTokenResult;

    public interface IGetTokenResult {
        void onSuccesfull(String accesToken);
        void onError(String msg);
    }

    public GetTokenTask(Context context, TokenPasswordRequestDto tokenPasswordRequestDto, IGetTokenResult iGetTokenResult) {
        this.context = context;
        this.tokenPasswordRequestDto = tokenPasswordRequestDto;
        this.iGetTokenResult = iGetTokenResult;
    }

    @Override
    protected String doInBackground(String... strings) {
        OAuth2Client client = new OAuth2Client.Builder(
                this.tokenPasswordRequestDto.getUsername(),
                this.tokenPasswordRequestDto.getPassword(),
                this.tokenPasswordRequestDto.getClient_id(),
                this.tokenPasswordRequestDto.getClient_secret(),
                this.tokenPasswordRequestDto.getUrl_connect_token()
        ).build();
        OAuthResponse response = null;
        try {
            response = client.requestAccessToken();
            if (response.isSuccessful()) {
                final String accessToken = response.getAccessToken();
                String refreshToken = response.getRefreshToken();
                iGetTokenResult.onSuccesfull(accessToken);

            } else {
                OAuthError error = response.getOAuthError();
                String errorMsg = error.getError();

                iGetTokenResult.onError(errorMsg);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
